<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\View\Compilers\Concerns\CompilesStacks;

class CompanyController extends Controller
{
    public function index(){
        $company = Company::all();
        return response()->json($company,200);
    }

    public function show($id){
        $company = Company::find($id);
        return response()->json($company, 200);
    }

    public function store(Request $request){
        $company = Company::create($request->all());
        return response()->json($company, 201);
    }

    public function delete( $id) {
        $company = Company::findOrFail($id);
        $company->delete();

        return response()->json(['data'=> null], 204);
    }

    public function checkToDelete( $id) {
        $company = Company::findOrFail($id);
        $employees = DB::table('employees')->where('com_id', $company->id)->first();
        if (is_null($employees)) {
            return response()->json(['data'=>'SUCCESS'], 200);
        } else {
            return response()->json(['data'=>'FAILED:Exist user with this company, please delete all of them before delete this company'], 200);
        }
    }

    public function update(Request $request, $id)
    {
        $company = Company::findOrFail($id);
        $company->update($request->all());

        return $company;
    }

}
